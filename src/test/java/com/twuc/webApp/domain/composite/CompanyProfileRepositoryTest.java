package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;

class CompanyProfileRepositoryTest extends JpaTestBase {
    @Autowired
    private CompanyProfileRepository companyProfileRepository;
    @Test
    void should_check_success(){
        assertTrue(true);
    }

    @Test
    void should_get_and_save_value(){
        CompanyProfile companyprofile = companyProfileRepository.save(new CompanyProfile("Xi'an", "天谷八路"));
        companyProfileRepository.flush();
        assertNotNull(companyprofile);
    }

    @Test
    void should_get_and_save_composite_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.save(new CompanyProfile("Chengdu", "春熙路"));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Chengdu", profile.getCity());
            assertEquals("春熙路", profile.getStreet());
        });
    }
}