package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserProfileRepositoryTest extends JpaTestBase {
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Test
    void should_get_and_save_value(){
        ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(entityManager -> {
            final UserProfile userprofile = userProfileRepository.save(new UserProfile("Shanxi", "gaoxin road"));
            expectedId.setValue(userprofile.getId());
        });
        run(em -> {
            final UserProfile profile =
                    userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Shanxi", profile.getAddressCity());
            assertEquals("gaoxin road", profile.getAddressStreet());
        });
    }
}
