package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class CompanyProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false,length = 128)
    private String city;
    @Column(nullable = false,length = 128)
    private String street;

    public CompanyProfile() {
    }

    public CompanyProfile(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }
}
